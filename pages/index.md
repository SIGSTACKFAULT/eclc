# Home

The Eastern Canada Lotus Club is, at present, a fairly loose association of about fifty Lotus enthusiasts from the area between Toronto, Ontario, Canada and England that get together several times a year.

We publish a newsletter, LOTUS TIMES, throughout the summer, with information about any upcoming get-togethers, a calendar of events and other news that could be of interest to fans of Lotus Cars.

The members have a vast experience with Lotus cars and are often willing to share their maintenance, repair, parts sources, experience and opinions with anyone who cares enough to listen.

## Editing this site

- [Markdown cheat sheet](https://devhints.io/markdown)