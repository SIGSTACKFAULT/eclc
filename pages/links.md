# Links

!!! warning "TODO"

    make these actual hyperlinks

## Lotus Car Clubs

- Club Lotus
- Club Lotus Northwest
- Golden Gate Lotus Club
- Historic Lotus Register
- Lotus Car Club of British Columbia
- Lotus Club of Canada
- Lotus Corps
- Lotus, Limited
- Lotus Owners of New York (LOONY)
- Lotus Seven Club
- Talbot Sunbeam Lotus Owners Club

## Lotus Related Sites

- Bibliography of Lotus Cars  (origin Mike Causer's Web Pages)
- LotusElan.net

## Local Car Clubs

- Boot'n Bonnet Car Club  Kingston, Ontario
- Ottawa Jaguar Club  Ottawa, Ontario
- Ottawa Valley Triumph Club  Ottawa, Ontario

## Services and Parts

- Air Craft Spruce and Specialty Co.
- Dave Bean Engineering
- Demon Tweeks
- JAE
- Lee Chapman Racing
- Little Britain Motor Company
- Martin MacGregor
- Paul Matty Sports Cars
- R.D. Enterprises
- Tony Thompson Racing
